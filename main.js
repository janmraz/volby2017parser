const json2csv = require('json2csv')
const fs = require('fs')

let RESULTS = require('./results.json')
const FILE_RESULT = 'result_percent.csv'

let kraj_example = {
  name: '',
  mandate: 1,
  valid_votes: 1,
  id: 1,
  ano: 1,
  ods: 1,
  pir: 1,
  spd: 1,
  kscm: 1,
  cssd: 1,
  kdu: 1,
  top: 1,
  stan: 1
}

let REGIONS = []

for(let i = 0; i < RESULTS['VYSLEDKY']['KRAJ'].length; i++){
  let kraj = {
    name: RESULTS['VYSLEDKY']['KRAJ'][i]['-NAZ_KRAJ'],
    id: "CZ" + i,
    mandate: parseInt(RESULTS['VYSLEDKY']['KRAJ'][i]['-POCMANDATU'], 10),
    valid_votes: parseInt(RESULTS['VYSLEDKY']['KRAJ'][i]['UCAST']['-PLATNE_HLASY'], 10)
  }
  for(let k = 0; k < RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'].length; k++){
    switch (RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k]['-KSTRANA']) {
      case '1':
        kraj['ods'] = parseFloat(RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k][ 'HODNOTY_STRANA']['-PROC_HLASU'])
        break;
      case '4':
        kraj['cssd'] = parseFloat(RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k][ 'HODNOTY_STRANA']['-PROC_HLASU'])
        break;
      case '7':
        kraj['stan'] = parseFloat(RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k][ 'HODNOTY_STRANA']['-PROC_HLASU'])
        break;
      case '8':
        kraj['kscm'] = parseFloat(RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k][ 'HODNOTY_STRANA']['-PROC_HLASU'])
        break;
      case '15':
        kraj['pir'] = parseFloat(RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k][ 'HODNOTY_STRANA']['-PROC_HLASU'])
        break;
      case '20':
        kraj['top'] = parseFloat(RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k][ 'HODNOTY_STRANA']['-PROC_HLASU'])
        break;
      case '21':
        kraj['ano'] = parseFloat(RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k][ 'HODNOTY_STRANA']['-PROC_HLASU'])
        break;
      case '24':
        kraj['kdu'] = parseFloat(RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k][ 'HODNOTY_STRANA']['-PROC_HLASU'])
        break;
      case '29':
        kraj['spd'] = parseFloat(RESULTS['VYSLEDKY']['KRAJ'][i]['STRANA'][k][ 'HODNOTY_STRANA']['-PROC_HLASU'])
        break;
      default:
        //didnt get into parliament
    }
  }
  REGIONS.push(kraj)
}

try {
  var result = json2csv({ data: REGIONS, fields: kraj_example.keys })
  fs.writeFile(FILE_RESULT, result, function(err) {
    if(err) {
        return console.log(err)
    }

    console.log('The file was saved into', FILE_RESULT)
  })
} catch (err) {
  // Errors are thrown for bad options, or if the data is empty and no fields are provided.
  // Be sure to provide fields if it is possible that your data array will be empty.
  console.error(err);
}
